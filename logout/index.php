<?php
	require_once('../templates/functions.php');
	session_start();
	session_unset();
	session_destroy();
	header('Location: ' . home_url());
	exit();
?>
