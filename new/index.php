<?php
        require_once('../templates/authentication.php');
	check_authentication();
	require_once('../templates/functions.php');
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$filename = $_FILES['uploaded_file']['name'];
		if (stripos(mime_content_type($_FILES['uploaded_file']['tmp_name']), 'image') !== false) {
			$file = file_get_contents($_FILES['uploaded_file']['tmp_name']);

			$picture_url = new_file($filename, $file);

			update_pictures_json($picture_url);

			header('Location: ' . $picture_url);
			exit();
		} else { exit(); }
	} else {
		echo file_get_contents('uploadForm.html');
		require("../templates/footer.php");
	}
?>
