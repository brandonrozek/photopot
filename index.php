<!Doctype html>
<html lang='en'>
<head>
	<meta charset='utf-8'/>
	<meta name='viewport' context='initial-scale=1,width=device-width'/>
	<?php require_once('templates/functions.php'); set_title(); ?>
	<link rel='stylesheet' href='/css/style.css'/>
	<?php
		if (isset($_GET['filters'])) {
			echo "<link rel='stylesheet' href='/css/filters.css'/>";
		}
	?>
</head>

<body style='margin:0;'>
	<script src='/js/jquery.js'></script>
	<script src='/js/collagePlus.js'></script>
	<script src='/js/home.js'></script>
	<main class='collage'>
		<?php
			require_once('templates/photo_functions.php');
			$json = json_decode(file_get_contents("photos/photos.json"), true);
			$random_pictures = randomPictures($json);
			$i = 0;
			foreach($random_pictures as $picture_url) {
				if ($i < 50) {
					$imageinfo = getimagesize($substr($picture_url, 1));
					if (isset($_GET['filters'])) {
						echo "<img src='$url' $imageinfo[3]  class='" . randomFilter() . "'/>";
					} else {
						echo "<img src='$url' $imageinfo[3]/>";
					}
					$i = $i + 1;
				} else {
					break;
				}
			}
		?>
	</main>
	<footer>
		<?php
			if (isset($_GET['filters'])) {
				echo "<a href='" . home_url() . "'>View Original Photos</a>";
			} else {
				echo "<a href='" . home_url() .  "/?filters'>Add random filters!</a>";
			}
		?>
	</footer>
	<?php require_once('templates/footer.php'); ?>
