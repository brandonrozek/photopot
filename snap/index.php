<?php
        require_once('../templates/authentication.php');
	check_authentication();
	require_once('../templates/functions.php');
	if ($_SERVER["REQUEST_METHOD"] === 'POST') {
		$path = key($_POST);
		$file = $_POST[$path];

		$file = get_canvas_data($file);

		$picture_url = new_file($path . '.png', $file);

		update_pictures_json($picture_url);

		header('Location: ' . $picture_url);
		exit();
	} else {
		echo file_get_contents('grabPicture.html');
		require("../templates/footer.php");
	}
?>
