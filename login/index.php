<?php
	require_once('../templates/functions.php');
	require_once('../templates/authentication.php');
	session_start();


	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
		if ($_SESSION['authenticated']) {
			header('Location: ' . home_url() . '/new/');
			exit();
		} else {
			if (isset($_GET['redirect'])) {
				$_SESSION['redirect'] = $_GET['redirect'];
			} else {
				session_unset();
				session_destroy();
			}
			echo file_get_contents('loginForm.html');
			require('footer.php');
			exit();
		}
	}
	elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if (verify_user($_POST['user'], $_POST['pass'])) {
			session_regenerate_id();
			$_SESSION['username'] = $_POST['user'];
			$_SESSION['authenticated'] = true;
			if (isset($_SESSION['redirect'])) {
				header('Location: ' . home_url() . $_SESSION['redirect']);
				unset($_SESSION['redirect']);
			} else {
				header('Location: ' . home_url() . '/new/');
			}
			exit();
		} else {
			header('Location: ' . home_url());
			exit();
		}
	}
?>
