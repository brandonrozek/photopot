var video = document.getElementsByTagName('video')[0];
var image = document.getElementsByTagName('img')[0];
var canvas = document.getElementsByTagName('canvas')[0];
var submit = document.getElementsByTagName('button')[0];

var context = canvas.getContext('2d');

var snapshot = function() {
	canvas.height = video.videoHeight;
	canvas.width = video.videoWidth;
	context.drawImage(video, 0, 0);
	image.src = canvas.toDataURL();
	video.style.display = 'none';
	image.style.display = 'block';
	submit.style.display = 'block';
}

var resume = function() {
	image.style.display = 'none';
	submit.style.display = 'none';
	video.style.display = 'block';	
}

var upload = function() {
	var now = Date.now();

	var formData = new FormData();
	formData.append(now, image.src.replace('data:image/png;base64,', ''));

	var request = new XMLHttpRequest();
	request.open("POST", "/snap/");
        request.onload = function(event) { window.location = "/photos/" + new Date().getFullYear() + "/" + ((new Date().getMonth() + 1 < 10)? "0" + String(new Date().getMonth() + 1): String(new Date().getMonth() + 1)) + "/" + now + ".png"; };
	request.send(formData);
}

video.addEventListener('click', snapshot, false);
image.addEventListener('click', resume, false);
submit.addEventListener('click', upload, false);

navigator.getUserMedia  = navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia;

navigator.getUserMedia({video: true}, function(stream) {
    	video.src = window.URL.createObjectURL(stream);
}, function() {});
