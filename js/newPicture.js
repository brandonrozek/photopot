// file drag hover
function FileDragHover(event) {
	event.stopPropagation();
	event.preventDefault();
}

// file selection
function FileSelectHandler(event) {
	FileDragHover(event);
	var files = event.target.files || event.dataTransfer.files;

	var formData = new FormData();
	formData.append('uploaded_file', files[0]);

	var request = new XMLHttpRequest();
	request.open("POST", "/new/");
	request.onload = function(event) { window.location = "/photos/" + new Date().getFullYear() + "/" + ((new Date().getMonth() + 1 < 10)? "0" + String(new Date().getMonth() + 1): String(new Date().getMonth() + 1)) + "/" + files[0].name; };
	request.send(formData);
}

var filedrag = document.getElementById('dropfile');

// file drop
filedrag.addEventListener("dragover", FileDragHover, false);
filedrag.addEventListener("dragleave", FileDragHover, false);
filedrag.addEventListener("drop", FileSelectHandler, false);
