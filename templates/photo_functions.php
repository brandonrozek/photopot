<?php
	function randomFilter() {
		$filters = array('grayscale', 'sepia', 'hue-rotate', 'contrast', 'blur');
		return $filters[array_rand($filters)];
	}

	function array_flatten($array) {
    	$return = array();
    	foreach ($array as $key => $value) {
    	    if (is_array($value)){
    	        $return = array_merge($return, array_flatten($value));
    	    } else {
    	        $return[$key] = $value;
    	    }
    	}

    	return $return;
	}

	function randomPictures($pictures) {
		$pictures = array_flatten($pictures);
		$shuffled = array();

		$picture_keys = array_keys($pictures);
		shuffle($picture_keys);

		foreach($picture_keys as $key) {
			$shuffled[$key] = $pictures[$key];
		}

		return $shuffled;
	}
?>
