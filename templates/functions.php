<?php
	$site_name = 'Photopot';

	function home_url() {
		return 'https://' . $_SERVER['HTTP_HOST'];
	}

	/*
		Outputs the title tag
		Ex: "Playlist | Meldic Radio"
	*/
	function set_title($stub) {
		if (isset($stub)) {
			echo '<title>' .  $stub . ' | ' . $GLOBALS["site_name"] . '</title>';
		} else {
			echo '<title>' . $GLOBALS["site_name"] . '</title>';
		}
	}

	function new_file($file_name, $file_contents) {
		
		$year = date('Y');
		$month = date('m');

		// Make /$year/$month directories if non-existent
		is_dir('../photos/' . $year) || mkdir('../photos/' . $year, 0750);
		is_dir('../photos/' . $year . '/' . $month) || mkdir('../photos/' . $year . '/' . $month, 0750);

		$file_name = '/photos/' . $year . '/' . $month . '/' . $file_name;

		file_put_contents('../' . $file_name, $file_contents);

		return home_url() . $file_name;
	}

	function get_canvas_data($data) {
		$data = str_replace(' ', '+', $data);
		return base64_decode($data);
	}

	function update_pictures_json($picture_url) {

		$path = parse_url($picture_url)["path"];

		$year = substr($path, strpos($path, '/photos/') + 8);
		$year = substr($year, 0, strpos($year, '/'));

		$month = substr($path, strpos($path, '/photos/' . $year . '/') + 9 + strlen($year));
		$month = substr($month, 0, strpos($month, '/'));

		$json = json_decode(file_get_contents('../photos/photos.json'), true);

		if (!isset($json[$year])) {
			$json[$year] = array();
		}
		if (!isset($json[$month])) {
			$json[$year][$month] = array();
		}

		array_push($json[$year][$month], pathinfo($picture_url)["basename"]);

		file_put_contents('../photos/photos.json', json_encode($json));
	}
?>
