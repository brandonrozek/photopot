<?php
	require_once('functions.php');
	/*
		Is the person a authorized user?
	*/
	function verify_user($username, $pass) {
		$hash = '$2y$12$Fzbf2LK0OWH1FX45oq3yxuiKvAAOsCM413C9TWyDUXE0./vZDyPE.';
		session_regenerate_id();
		return $username === 'rozek_admin' && password_verify($pass, $hash);
	}
	/*
		Check if person is authorized to be on this page
		If not, then redirect to login
	*/
	function check_authentication() {
		session_start();
		if (!$_SESSION["authenticated"]) {
			session_unset();
			session_destroy();
			header('Location: ' . home_url() . '/login/?redirect=' . $_SERVER['REQUEST_URI']);
			exit();
		}
	}
?>
